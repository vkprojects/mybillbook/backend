FROM maven:3-eclipse-temurin-17 AS build
COPY . .
#RUN #mvn clean package -DskipTests

FROM openjdk:17.0.1-jdk-slim
COPY --from=build /target/MyBillBook-0.0.1-SNAPSHOT.jar mybillbook.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","mybillbook.jar"]