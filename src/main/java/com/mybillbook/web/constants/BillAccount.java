package com.mybillbook.web.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@Getter
public enum BillAccount {
    UnUpdatables(new ArrayList<>(Arrays.asList("accountCreatedOn"))),
    SearchAttributes(new ArrayList<>(Arrays.asList("accountId","accountUserList")));

    private List<String> attributes;

}
