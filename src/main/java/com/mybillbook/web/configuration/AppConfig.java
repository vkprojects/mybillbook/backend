package com.mybillbook.web.configuration;

import com.customjar.security.exceptionHandler.CustomAuthenticationFailureHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {

    public
    @Bean RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler(){return new CustomAuthenticationFailureHandler();}

/*
    @Bean
    public CustomAuthenticationEntryPoint customAuthenticationEntryPoint(){return new CustomAuthenticationEntryPoint();}
*/

}
