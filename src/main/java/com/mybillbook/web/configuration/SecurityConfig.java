package com.mybillbook.web.configuration;

import com.customjar.security.filters.CustomJWTTokenFilterValidator;
import com.customjar.security.services.TokenValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import java.util.Arrays;

//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Arrays;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {


    @Autowired
    @Qualifier("customAuthenticationEntryPoint")
    AuthenticationEntryPoint authEntryPoint;

    @Autowired
    AuthenticationFailureHandler authenticationFailureHandler;

//    @Bean
//    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
//            throws Exception {
//        return authenticationConfiguration.getAuthenticationManager();
//    }

//    private final AuthenticationProvider authenticationProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

//        AuthenticationManagerBuilder authenticationManagerBuilder = http.getSharedObject(AuthenticationManagerBuilder.class);



                http
                        .csrf().disable()
                        .cors().configurationSource(request -> {
                            CorsConfiguration corsConfiguration = new CorsConfiguration();
                            corsConfiguration.setAllowedOrigins(Arrays.asList("*"));
                            corsConfiguration.setAllowedMethods(Arrays.asList("GET","PUT","POST","DELETE","OPTIONS"));
                            corsConfiguration.setAllowedHeaders(Arrays.asList("*"));
                            corsConfiguration.setExposedHeaders(Arrays.asList("Content-Disposition"));
                            return corsConfiguration;
                        })
                        .and()
                        .sessionManagement()
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                        .and()
                        .exceptionHandling()
                        .authenticationEntryPoint(authEntryPoint)
                        .accessDeniedHandler((req, res, e)->{
                            System.out.println("My Custom error");
                            res.getOutputStream().println("My custom error: "+e);
                        })
                        .and()
//                        .authorizeHttpRequests()
/*                        .requestMatchers(
                                "/v2/api-docs",
                                "/v3/api-docs",
                                "/v3/api-docs/**",
                                "/swagger-resources",
                                "/swagger-resources/**",
                                "/configuration/ui",
                                "/configuration/security",
                                "/swagger-ui/**",
                                "/webjars/**",
                                "/swagger-ui.html",
                                "/api/v1/auth/**", "/v1/signUp"
                        ).permitAll()*/
//                        .requestMatchers("/api/v1/auth/**", "/v1/signUp").permitAll()
//                        .anyRequest().authenticated()
//                        .and()
                        .addFilterBefore(
                                new CustomJWTTokenFilterValidator(
                                        new TokenValidator("abc"),
                                        Arrays.asList(
                                                "/v2/api-docs",
                                                "/v3/api-docs",
                                                "/v3/api-docs/**",
                                                "/swagger-resources",
                                                "/swagger-resources/**",
                                                "/configuration/ui",
                                                "/configuration/security",
                                                "/swagger-ui/**",
                                                "/webjars/**",
                                                "/swagger-ui.html",
                                                "/api/v1/auth/**",
                                                "/v1/signUp",
                                                "/v1/signIn",
                                                "/v1/generateToken",
                                                "/search/v2",
                                                "/actuator",
                                                "/actuator/caches"
                                        ),
                                        authenticationFailureHandler
                                ),
                                BasicAuthenticationFilter.class
                        );
        return http.build();
    }

}
