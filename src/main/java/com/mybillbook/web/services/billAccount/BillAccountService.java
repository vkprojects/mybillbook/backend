package com.mybillbook.web.services.billAccount;

//import com.mybillbook.web.model.BillAccount.BillAccountDto;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mybillbook.web.model.BillAccount.BillAccountDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface BillAccountService {

    BillAccountDto save(BillAccountDto billAccount);
    BillAccountDto getAccountAdmins(String billAccountNumber);
    Optional<BillAccountDto> getBillAccountById(String billAccountNumber);

    UpdateResult updateAccount(String billAccountNumber, Map<String, Object> upadtedParams);

    DeleteResult deleteAccount(String billAccountNumber);

//    List<BillAccountDto> searchBillAccount(String _id, String accountAdmin);

    List<BillAccountDto> searchBillAccount(HashMap<String, Object> searchReq);

/*    @Cacheable(value = "searchcache", key = "#x")
    String getCachedBillAccount1(String x);*/

    String searchBillAccount1(String x) throws InterruptedException;

//    List<BillAccountDto> searchBillAccountWithId(String _id);

//    List<BillAccountDto> searchBillAccountWithUser(List<String> user);

}
