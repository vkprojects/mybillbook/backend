package com.mybillbook.web.services.billAccount;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mybillbook.web.constants.BillAccount;
import com.mybillbook.web.model.BillAccount.BillAccountDto;
import com.mybillbook.web.repository.BillAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BillAccountServiceImpl implements BillAccountService{

    @Autowired
    BillAccountRepository billAccountRepository;

//    @Autowired
//    MongoOperations mongoOperations;

    @Autowired
    MongoTemplate mongoTemplate;

//    MongoTemplate
    @Override
    public BillAccountDto save(BillAccountDto billAccount) {
       return billAccountRepository.save(billAccount);
    }

    @Override
    public BillAccountDto getAccountAdmins(String billAccountNumber) {

        Query selectQuery = new Query();
        selectQuery.addCriteria(Criteria.where("_id").is(billAccountNumber));
        return billAccountRepository.getAccountAdmins(billAccountNumber);
    }



    @Override
    public Optional<BillAccountDto> getBillAccountById(String billAccountNumber) {
        return billAccountRepository.findById(billAccountNumber);
    }

    @Override
    public UpdateResult updateAccount(String billAccountNumber, Map<String, Object> upadtedparams) {

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(billAccountNumber));
        Update updateQuery = new Update();

        for (String key: upadtedparams.keySet()){
            if(BillAccount.UnUpdatables.getAttributes().stream().anyMatch(key::equalsIgnoreCase)){
                upadtedparams.remove(key);
            }
            updateQuery.set(key, upadtedparams.get(key));
        }

        UpdateResult updateResult = mongoTemplate.updateMulti(query, updateQuery, BillAccountDto.class);

//        BillAccountDto updated = billAccountRepository.updateAccount(billAccountNumber, upadtedparams);
//        return Optional.ofNullable(updated);
        return updateResult;
    }

    @Override
    public DeleteResult deleteAccount(String billAccountNumber) {

        Query selectQuery = new Query();
        selectQuery.addCriteria(Criteria.where("_id").is(billAccountNumber));

        DeleteResult deleteResult = mongoTemplate.remove(selectQuery, BillAccountDto.class);
        return deleteResult;
    }

    @Override
    public List<BillAccountDto> searchBillAccount(HashMap<String,Object> searchReq) {
//        List<BillAccountDto> res = billAccountRepository.findByFields(Arrays.asList(_id, user));
        List<BillAccountDto> searchResult = new ArrayList<>();
        Query findQuery = new Query();
        for(String key: searchReq.keySet()){
            if (searchReq.get(key)!= null) {
                if(key.contains("List")){
                    findQuery.addCriteria(Criteria.where(key).elemMatch(new Criteria().in(searchReq.get(key))));
//                    findQuery.addCriteria(Criteria.where(key).in(Arrays.asList(searchReq.get(key))));
                }
                else if(key.equalsIgnoreCase("accountId")){
                    findQuery.addCriteria(Criteria.where("_id").is(searchReq.get(key)));
                }
                else {
                    findQuery.addCriteria(Criteria.where(key).is(searchReq.get(key)));
                }
            }
        }
//        if(searchReq.containsKey("accountId") && searchReq.containsKey("accountUser")){
//            findQuery.
//        }
//        if(!(_id.isBlank() || user.isBlank())){
//            findQuery.addCriteria(Criteria.where("_id").is(_id));
//            findQuery.addCriteria(Criteria.where("accountHolders").in(Arrays.asList(user)));
//        }
//        else if(!_id.isBlank()){
//            findQuery.addCriteria(Criteria.where("_id").is(_id));
//        }
//        else if (!user.isBlank()) {
//            findQuery.addCriteria(Criteria.where("accountHolders").in(Arrays.asList(user)));
//        }
//        else{
//            return null;
//        }

//        findQuery.addCriteria(Criteria.where("_id").is(_id));
//        findQuery.addCriteria(Criteria.where("accountHolders").elemMatch(new Criteria().in(user)));
//        findQuery.addCriteria(Criteria.where("accountAdmin").elemMatch(Criteria.));
//        Query q = Query.query(where(_id).in(Arrays.asList(ids))
//                .andOperator(new Criteria()
//                        .orOperator(
//                                where(ENTRIES).elemMatch(where("projectId").is(projectName)))));
//        findQuery.addCriteria(Criteria.where("accountAdmin").elemMatch(Criteria.));
//        findQuery.fields().
        searchResult = mongoTemplate.find(findQuery,BillAccountDto.class);
//        System.out.println("Result got");
        return searchResult;
    }


    @Override
    public String searchBillAccount1(String x) {
        // This is the original method with the delay
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return x;
    }

//    @Override
//    public List<BillAccountDto> searchBillAccountWithId(String _id) {
//
//        Query searchQuery  = new Query();
//        searchQuery.addCriteria(Criteria.where("_id").is(_id));
//        List<BillAccountDto> searchResult = mongoTemplate.find(searchQuery, BillAccountDto.class);
//        return searchResult;
//    }
//
//    @Override
//    public List<BillAccountDto> searchBillAccountWithUser(List<String> user) {
//        return null;
//    }
}
