package com.mybillbook.web.services.transaction;


import com.mongodb.client.result.UpdateResult;
import com.mybillbook.web.model.Trsnsaction.TransactionDto;
import com.mybillbook.web.model.Trsnsaction.TransactionList;
import com.mybillbook.web.repository.TransactionRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService{

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public TransactionDto createTransaction(TransactionDto transactionDto) {
        return transactionRepository.save(transactionDto);
    }

    @Override
    public UpdateResult addNewTransactionEntry(String billAccountId, List<TransactionList> transactionList) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        for(TransactionList transaction: transactionList){
            String strDate = transaction.getTransactionDate().substring(0,10);
//            LocalDate transactionDate = LocalDate.parse(strDate);
//            transaction.setTransactionDate(LocalDate.parse(strDate, formatter));
            transaction.setTransactionDate(strDate);
            transaction.setDateOfEntry(LocalDateTime.now());
        }
        Query query = new Query(Criteria.where("_id").is(billAccountId));
        Update addNewTransactionQuery = new Update();
        addNewTransactionQuery.push("transactionList").each(transactionList); // add multiple elements to the array
        UpdateResult updateResult = mongoTemplate.updateFirst(query, addNewTransactionQuery, TransactionDto.class);
        return updateResult;
    }

    @Override
    public List<TransactionDto> getTransactionsWIthIds(List<String> transactionIds, String accountId) {
        List<ObjectId> objids = transactionIds.stream().map(ObjectId::new).collect(Collectors.toList());
        Query query = new Query(Criteria.where("_id").is(new ObjectId(accountId)));
        query.addCriteria(Criteria.where("transactionList").in(objids));
        List<TransactionDto> searchResult = mongoTemplate.find(query,TransactionDto.class);
        return searchResult;
    }

    @Override
    public UpdateResult  deleteTransactions(String accountId, List<String> transactionIds) {
        List<ObjectId> objids = transactionIds.stream().map(ObjectId::new).collect(Collectors.toList());
        Query query = new Query(Criteria.where("_id").is(new ObjectId(accountId)));
        Update update = new Update().pull("transactionList", Query.query(Criteria.where("_id").in(objids)));
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, TransactionDto.class);
        return updateResult;
    }

    @Override
    public TransactionDto getAllTransactions(String accountId) {
        Query searchQuery = new Query(Criteria.where("_id").is(new ObjectId(accountId)));
        searchQuery.with(Sort.by(Sort.Direction.ASC, "transactionList.dateOfEntry"));
//        TransactionDto searchResult = mongoTemplate.findById(new ObjectId(accountId), TransactionDto.class);
        List<TransactionDto> searchResult = mongoTemplate.find(searchQuery, TransactionDto.class);
        return searchResult.get(0);
    }
}
