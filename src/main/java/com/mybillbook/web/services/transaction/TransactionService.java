package com.mybillbook.web.services.transaction;


import com.mongodb.client.result.UpdateResult;
import com.mybillbook.web.model.Trsnsaction.TransactionDto;
import com.mybillbook.web.model.Trsnsaction.TransactionList;

import java.text.ParseException;
import java.util.List;

public interface TransactionService {


    public TransactionDto createTransaction(TransactionDto transactionDto);

    public UpdateResult addNewTransactionEntry(String billlAccountId, List<TransactionList> transactionList) throws ParseException;
//    @Query()
    public List<TransactionDto> getTransactionsWIthIds(List<String> transactionIds, String accountId);

    public UpdateResult deleteTransactions(String accountids, List<String> transactionIds);

    public TransactionDto getAllTransactions(String accountId);
}
