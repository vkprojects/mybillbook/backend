package com.mybillbook.web.services.user;

import com.mybillbook.web.model.User.User;
import com.mybillbook.web.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements Userservices {

    @Autowired
    UserRepository userRepository;

    @Override
    public User save(User user) {
        if(userRepository.existsById(user.getEmail())){
            return null;
        }
//        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        String encodedPassword = passwordEncoder.encode(user.getPassword());
//        user.setPassword(encodedPassword);
        return userRepository.save(user);
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepository.findById(email);
    }


    @Override
    public boolean ifUsersExists(List<String> emailList) {
        List<User> userList = userRepository.isUsersExist(emailList);
        if(emailList.size() != userList.size()){
            return false;
        }
        return true;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userInfo = getUserByEmail(username);
        return userInfo.map(user -> new User())
                .orElseThrow(() -> new UsernameNotFoundException("user not found " + username));
    }
}
