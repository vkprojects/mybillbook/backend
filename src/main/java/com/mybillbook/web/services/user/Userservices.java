package com.mybillbook.web.services.user;

import com.mybillbook.web.model.User.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface Userservices extends UserDetailsService {
    User save(User user);
//    Optional<User> signIn(String email);

    Optional<User> getUserByEmail(String email);

//    boolean ifUserExists(List<String> emailList);

    boolean ifUsersExists(List<String> emailList);

//    Optional<User> getUser(String userName);
}
