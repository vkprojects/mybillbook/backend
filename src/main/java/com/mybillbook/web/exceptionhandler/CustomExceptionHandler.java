package com.mybillbook.web.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<String> handleNullPointerException(NullPointerException ex) {
        // Handle the NullPointerException here
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred");
    }

/*    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<String> handleNullPointerException(NullPointerException ex) {
        // Create a custom error message or perform any other actions
        String errorMessage = "A NullPointerException occurred: " + ex.getMessage();

        // Return an appropriate HTTP response with the error message
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
    }*/

/*    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleInternalErrors(NullPointerException ex){
        StringBuilder message = new StringBuilder();
        System.out.println("The Exception Message :"+ex);
        if(ex.getMessage().contains("No Bearer Token found")){
            message.append(ex.getMessage());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(message.toString());
    }*/

/*    @ExceptionHandler(CustomNullPointerException.class)
    public ResponseEntity<String> handleNullPointerException(CustomNullPointerException ex) {
        // Create a custom error message or perform any other actions
        System.out.println("hello this is an NullPonter exception");
        String errorMessage = "A NullPointerException occurred: " + ex.getMessage();
        // Return an appropriate HTTP response with the error message
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
    }*/


/*    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception ex) {
        // Create a custom error message or perform any other actions
        String errorMessage = "An error occurred: " + ex.getMessage();
        // Return an appropriate HTTP response with the error message
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
    }*/

/*    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<String> handleFilterException(NullPointerException ex) {
        // Customize the response or error handling based on your requirements
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("An error occurred during filtering: ");
    }*/
}
