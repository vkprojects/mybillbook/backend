package com.mybillbook.web.exceptionhandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybillbook.web.model.Response.CustomResponse;
import com.mybillbook.web.model.Response.ResponseDetails;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;

@Component("customAuthenticationEntryPoint")
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException, ServletException {

        CustomResponse res = new  CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails("Update failed", authException.getMessage()));
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        OutputStream responseStream = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(responseStream, res);
        responseStream.flush();
    }
}

/*
@Component("customAuthenticationEntryPoint")
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        System.out.println("Hello this is auth handler");
        CustomResponse res = new  CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails("Update failed", authException.getMessage()));
//        RestError re = new RestError(HttpStatus.UNAUTHORIZED.toString(), "Authentication failed");
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        OutputStream responseStream = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(responseStream, res);
        responseStream.flush();

        // Custom handling for authentication exceptions
*/
/*        if (authException instanceof NullPointerException) {
            // Handle specific custom exception
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Custom Authentication Exception");
        }
        else {
            // Handle other authentication exceptions
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        }*//*

    }
}

*/
