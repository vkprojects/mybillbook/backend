package com.mybillbook.web.model.BillAccount;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("BillAccount")
@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public class BillAccountDto {

    //validated values
    @Id
    private String accountId;


    @NotNull(message = "Account name cannot be null")
    @NotBlank(message = "Account name cannot be blank")
    @Size(max = 10, message = "maximum length exceeded")
    private String accountName;

    @NotNull(message = "Account type cannot be null")
    @NotBlank(message = "Account type cannot be blank")
    private String accountType;

    @NotNull(message = "Account Admin list cannot be null")
    @Size.List({@Size(min = 1, message = "Account Admin list cannot be empty")})
    private List<String> accountAdminList;

    @NotNull(message = "Account User list cannot be null")
    @Size.List({@Size(min = 1, message = "Account User list cannot be empty")})
    private List<String> accountUserList;

    //set on creation
    private String accountCreatedOn;
//    private TransactionDto transaction;
}
