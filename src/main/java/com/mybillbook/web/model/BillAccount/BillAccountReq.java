package com.mybillbook.web.model.BillAccount;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.Map;

@Data
public class BillAccountReq {
    @NotNull(message = "accountId cannot be null")
    @NotBlank(message = "accountId cannot be blank")
    private String billAccountId;
    private Map<String, Object> billAccount;
}
