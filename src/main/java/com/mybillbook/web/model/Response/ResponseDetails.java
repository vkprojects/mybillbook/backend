package com.mybillbook.web.model.Response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseDetails {
    private Object message;
    private Object details;
}
