package com.mybillbook.web.model.Response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomResponse {
        private final Integer status;
        private ResponseDetails success;
        private Object error;
}
