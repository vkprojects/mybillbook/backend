package com.mybillbook.web.model.User;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;


@Data
@NoArgsConstructor
@ToString(exclude = "password")
@Document("User")
public class User implements UserDetails{


    //validated attributes
    @Id
    @Email(message = "Invalid Email")
    private String email;
    @Size(min = 8, max = 12, message = "Password doesnot meet the criteria")
    protected String password;

    //Default Empty Attributes
    @Value("")
    private String country;
    @Value("")
    private String address;
    @Value("")
    private Integer pin;
    @Value("")
    private String phoneNumber;
    @Value("")
    private String ip;
    @Value("")
    private name name;

    //set on creation attributes`
    private LocalDateTime userCreatedOn;

    //unset attributes
    private Date dob;

//    public User(User user) {
//    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
