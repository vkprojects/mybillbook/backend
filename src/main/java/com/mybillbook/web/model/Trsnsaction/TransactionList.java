package com.mybillbook.web.model.Trsnsaction;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
public class TransactionList {

    @Id
    private String transactionId;
    private String transactionFrom;
    private String transactionTo;
    private String transDirection;
    private String transactionType;
    private LocalDateTime dateOfEntry;
    private String transactionDate;
    private String transactionBank;
    private String transactionAmount;
    private String description;
}
