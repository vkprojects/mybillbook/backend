package com.mybillbook.web.model.Trsnsaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("AccountTransaction")
public class TransactionDto {

//    @Id
    @Id
    @JsonProperty("billAccountId")
    private String linkedBillAccount;
    private List<TransactionList> transactionList;

}
