package com.mybillbook.web.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("BankDetails")
public class BankDetails {
    @Id
    private String bankId;
    private String bankName;
    private String bankAccountType;
    private List<String> bankAccountHolders;
    private Long bankAccountNumber;
}
