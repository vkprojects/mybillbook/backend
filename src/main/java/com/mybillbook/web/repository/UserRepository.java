package com.mybillbook.web.repository;

import com.mybillbook.web.model.User.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    @Query(value = "{'email':?0}")
    User isUserExist(String email);

    @Query(value = "{_id :{$in: ?0}}")
    List<User> isUsersExist(List<String> emails);
}
