package com.mybillbook.web.repository;

import com.mybillbook.web.model.BillAccount.BillAccountDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Map;


public interface BillAccountRepository extends MongoRepository<BillAccountDto,String> {

    @Query(value = "{_id:ObjectId(?0)}", fields = "{_id:0, 'accountAdmin':1}")
    BillAccountDto getAccountAdmins(String billAccountId);

    @Query(value = "{_id:ObjectId(?0)}, {$set: ?0}")
    BillAccountDto updateAccount(String billId, Map<String, Object> updateparams);

    @Query("{field: {$in: ?0}}")
    List<BillAccountDto> findByFields(List<Object> fields );

//    List<BillAccountDto> findByFields(String _id, String accountName);
}
