package com.mybillbook.web.repository;

import com.mybillbook.web.model.Trsnsaction.TransactionDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface TransactionRepository extends MongoRepository<TransactionDto,String> {

    @Query("{{_id: ?0},{$pull: {transactionList: {_id: {$in: ?1 } }}}}")
    List<TransactionDto>  deleteTransactionWithIds(String accountId, List<String> transactionIds);
}
