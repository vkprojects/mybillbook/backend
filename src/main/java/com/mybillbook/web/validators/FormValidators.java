package com.mybillbook.web.validators;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormValidators {

    public static boolean isValidIp(String ip){
        String ipRegx = "(^((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)\\.?\\b){4}$)";
//        String ipRegx = _0To255 + "\\."+ _0To255 + "\\." + _0To255;
        Pattern pattern = Pattern.compile(ipRegx);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }

    public static boolean isValidName(String name) {
        return (name.length() <= 20);
    }

    public static boolean isValidLocation(List<String>locations, String location) {
        return (locations.contains(location));
    }

    public static boolean isValidPassword(String password) {
        int strength =10;
//      Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
        String passwordRegx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*?#&]{8,}$";
        Pattern pattern = Pattern.compile(passwordRegx);
        Matcher matcher = pattern.matcher(password);
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(strength, new SecureRandom());
//        String encodedPassword = bCryptPasswordEncoder.encode(password);
        return matcher.matches();
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return phoneNumber.split(" ")[1].length()==10;
    }
    public static boolean isValidDob(String dob) {
        String dobRegx = "[1-31]-[0-12]-[0-9][0-9][0-9][0-9]";
        Pattern pattern = Pattern.compile(dobRegx);
        Matcher matcher = pattern.matcher(dob);
        if(!matcher.matches()){
            return false;
        }
        return true;
    }

    //change
    public static boolean isValidDate(String dob) {
        String dobRegx = "[1-31]-[0-12]-[0-9][0-9][0-9][0-9]";
        Pattern pattern = Pattern.compile(dobRegx);
        Matcher matcher = pattern.matcher(dob);
        if(!matcher.matches()){
            return false;
        }
        return true;
    }
}
