package com.mybillbook.web.controller;

import com.customjar.security.services.TokenService;
import com.mybillbook.web.model.Response.CustomResponse;
import com.mybillbook.web.model.Response.ResponseDetails;
import com.mybillbook.web.model.User.User;
import com.mybillbook.web.services.user.Userservices;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.isNull;
//import static java.util.Objects.isB

@RestController
@RequestMapping("/v1")
public class UserController {

    @Autowired
    Userservices userservices;

    @Value("#{'${location.list}'.split(',')}")
    private List<String> locations;

    @Autowired
    Environment env;

//    @Autowired
//    FormValidators formValidators;

    //    @CrossOrigin
    @PostMapping("/signUp")
    private ResponseEntity<?> save(@Valid @RequestBody User user) {
        try {
            user.setUserCreatedOn(LocalDateTime.now());
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String encodedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(encodedPassword);
            User serviceResponse = userservices.save(user);
            if (isNull(serviceResponse)) {
                throw new Exception("User Already Exist");
            }

            CustomResponse customResponse = new CustomResponse(200, new ResponseDetails("User Successfully created", serviceResponse), null);
            return ResponseEntity.ok().body(customResponse);
        } catch (Exception e) {
            CustomResponse errorResponse = new CustomResponse(400, null, new ResponseDetails(e.getMessage(), null));
            return ResponseEntity.status(400).body(errorResponse);
        }
    }

    //    @CrossOrigin("*")
    @RequestMapping(method = RequestMethod.POST, value = "/signIn", consumes = "application/json", produces = "application/json")
    private ResponseEntity<?> signIn(@RequestBody HashMap<String, String> reqParams) throws RestClientException {
//        if(true){
//            throw new NullPointerException();
//        }
        try {
            User dbUser = userservices.getUserByEmail(reqParams.get("email")).orElseThrow(() ->
                    new UsernameNotFoundException("Login Unsuccessful User doesnot exist")
            );

            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//            if(!dbUser.getPassword().equals(passwordEncoder.encode(user.getPassword()))){
//                throw new Exception("Login Unsuccessful Incorrect password");
//            }
            if (!passwordEncoder.matches(reqParams.get("password"), dbUser.getPassword())) {
                throw new Exception("Login Unsuccessful Incorrect password");
            }

            TokenService tokenService = new TokenService();
            Map token = tokenService.generateToken(env.getProperty("secrete.key"));
            HashMap<String, Object> userWithToken = new HashMap<>();
            userWithToken.put("userData", dbUser);
            userWithToken.put("tokenDetails", token);
            CustomResponse successResponse = new CustomResponse(200, new ResponseDetails("Login Successful", userWithToken), null);
            return ResponseEntity.ok().body(successResponse);
        } catch (Exception e) {
            CustomResponse errorResposne = new CustomResponse(400, null, new ResponseDetails(e.getMessage(), null));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResposne);
        }
    }


    //    @CrossOrigin
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    private ResponseEntity<?> searchUser(@RequestParam(value = "email", required = false) String email,
                                         @RequestParam(value = "country", required = false) String country
    ) {
        try {
//            if(!email.isBlank() && !country.isBlank()){
//            }
            if (email.isBlank()) {
                throw new Exception("Mandatory Attribute email not found");
            }

            User user = userservices.getUserByEmail(email).orElseThrow(() ->
                    new UsernameNotFoundException("User not found")
            );
            CustomResponse successResponse = new CustomResponse(200, new ResponseDetails("User found", user), null);
            return ResponseEntity.status(HttpStatus.OK).body(successResponse);
        } catch (Exception e) {
            CustomResponse errorResposne = new CustomResponse(400, null, new ResponseDetails(e.getMessage(), null));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResposne);
        }
    }

    //    @CrossOrigin
    @GetMapping(path = "/generateToken", produces = "application/json")
    ResponseEntity<?> generateToken(
            @RequestParam(value = "user") String user
    ) {

        TokenService tokenService = new TokenService();
        Map token = tokenService.generateToken("abc");
        CustomResponse successResponse = new CustomResponse(200, new ResponseDetails("Request Successfull", token), null);
        return ResponseEntity.ok(successResponse);
    }
}