package com.mybillbook.web.controller;

import com.mybillbook.web.model.Response.CustomResponse;
import com.mybillbook.web.model.Response.ResponseDetails;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ValidationErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleValidationException(MethodArgumentNotValidException exp){

        Map<String,String> errors = new HashMap<>();
        exp.getBindingResult().getAllErrors().forEach((error)->{
            String fieldName = ((FieldError)error).getField();
            String message =error.getDefaultMessage();
            errors.put(fieldName,message);
        });
        CustomResponse customResponse = new CustomResponse(400, null, new ResponseDetails("Invalid input", errors));
        return ResponseEntity.status(400).body(customResponse);
    }
}
