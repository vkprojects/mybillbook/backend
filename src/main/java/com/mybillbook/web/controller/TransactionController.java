package com.mybillbook.web.controller;

//import com.mybillbook.web.model.Trsnsaction.TransactionDetails;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.result.UpdateResult;
import com.mybillbook.web.model.Response.CustomResponse;
import com.mybillbook.web.model.Response.ResponseDetails;
import com.mybillbook.web.model.Trsnsaction.TransactionDto;
import com.mybillbook.web.services.transaction.TransactionService;
import jakarta.servlet.http.HttpServletRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/transaction")
@CrossOrigin(origins = "http://localhost:3000")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @Autowired
    private RestTemplate restTemplate;




    @CrossOrigin("*")
    @PostMapping(value = "/add")
    private ResponseEntity<?> saveTransaction(
            @RequestHeader(name = "user") String user,
            @RequestBody TransactionDto transactionDto,
            HttpServletRequest httpServletRequest){

        try{
            URI uri = new URI("http://localhost:8080"+"/account/search?accountId="+transactionDto.getLinkedBillAccount());
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", httpServletRequest.getHeader("Authorization"));
            HttpEntity<String> entity = new HttpEntity<>(headers);
            ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET,entity, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            HashMap<String,Object> responseJSON = objectMapper.readValue(response.getBody(), HashMap.class);

            if(response.getStatusCode()!=HttpStatus.OK){
                throw new Exception(responseJSON.get("error").toString());
            }


            List<HashMap<String,Object>> billAccountDtos = (List<HashMap<String, Object>>) ((Map<String, Object>)responseJSON.get("success")).get("details");
            ArrayList<String> accountUserList = (ArrayList<String>) billAccountDtos.get(0).get("accountUserList");


            //            List<String> accountUserList = Collections.singletonList(billAccountDtos.get(0).get("accountUserList"));

            //            List<String> accountUserList = new ArrayList<>(Arrays.asList(billAccountDtos.get(0).get("accountUserList").toString()));
            if(!accountUserList.contains(user)){
                throw new Exception("User does not have access to set Transaction");
            }

            transactionDto.getTransactionList().forEach(transaction ->{
                ObjectId id= new ObjectId();
                transaction.setTransactionId(id.toString());
            });

            UpdateResult updateResult = transactionService.addNewTransactionEntry(transactionDto.getLinkedBillAccount(), transactionDto.getTransactionList());
            if(updateResult.getMatchedCount()==0){
                throw new Exception("Invalid account id");
            }
            if(updateResult.getMatchedCount()==0){
                throw new Exception("Transaction not updated");
            }
            return ResponseEntity.ok().body(new CustomResponse(200, new ResponseDetails("Transaction Created Successfully", updateResult), null));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails(e.getMessage(), null)));
        }
    }




    @CrossOrigin("*")
    @PostMapping(value = "/add/v2")
    private ResponseEntity<?> saveTransactionV2(
            @RequestHeader(name = "user") String user,
            @RequestBody TransactionDto transactionDto,
            HttpServletRequest httpServletRequest){

        try{
            URI uri = new URI("http://localhost:8080"+"/account/search?accountId="+transactionDto.getLinkedBillAccount());
            ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            HashMap<String,Object> responseJSON = objectMapper.readValue(response.getBody(), HashMap.class);

            if(response.getStatusCode()!=HttpStatus.OK){
                throw new Exception(responseJSON.get("error").toString());
            }


            List<HashMap<String,Object>> billAccountDtos = (List<HashMap<String, Object>>) ((Map<String, Object>)responseJSON.get("success")).get("details");
            ArrayList<String> accountUserList = (ArrayList<String>) billAccountDtos.get(0).get("accountUserList");


            //            List<String> accountUserList = Collections.singletonList(billAccountDtos.get(0).get("accountUserList"));

            //            List<String> accountUserList = new ArrayList<>(Arrays.asList(billAccountDtos.get(0).get("accountUserList").toString()));
            if(!accountUserList.contains(user)){
                throw new Exception("User does not have access to set Transaction");
            }

            transactionDto.getTransactionList().forEach(transaction ->{
                ObjectId id= new ObjectId();
                transaction.setTransactionId(id.toString());
            });

            UpdateResult updateResult = transactionService.addNewTransactionEntry(transactionDto.getLinkedBillAccount(), transactionDto.getTransactionList());
            if(updateResult.getMatchedCount()==0){
                throw new Exception("Invalid account id");
            }
            if(updateResult.getMatchedCount()==0){
                throw new Exception("Transaction not updated");
            }
            return ResponseEntity.ok().body(new CustomResponse(200, new ResponseDetails("Transaction Created Successfully", updateResult), null));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails(e.getMessage(), null)));
        }
    }
//    boolean isAdminExist = userservices.ifUsersExists(billAccount.getAccountAdmin());
//            if(!isAdminExist){
//        throw new UsernameNotFoundException("User Admin doesnot exist!");
//    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<?> deleteTransaction(
            @RequestHeader(name = "user") String user,
            @RequestBody HashMap<String, Object> deleteRequest){
        try {
            if(!deleteRequest.containsKey("accountId") || deleteRequest.get("accountId").toString().isEmpty()){
                throw new Exception("Mandatory accountId is not valid or nor passed");
            }

            if(!deleteRequest.containsKey("transactionIds")){
                throw new Exception("Mandatory transactionIds is not valid or nor passed");
            }
            List<String> transactionIds = (List<String>) deleteRequest.get("transactionIds");
            UpdateResult updateResult = transactionService.deleteTransactions(deleteRequest.get("accountId").toString(), transactionIds);
            return ResponseEntity.ok().body(new CustomResponse(200, new ResponseDetails("Transaction(s) Deleted", updateResult), null));
        }
        catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails(e.getMessage(), null)));
        }
    }

    @CrossOrigin("*")
    @GetMapping(value = "/getAllTransactions/{accountId}")
    private ResponseEntity<?> getAllTransactions(
            @RequestHeader(name = "user") String user,
            @PathVariable(name = "accountId") String accountId,
            HttpServletRequest httpServletRequest){
        try {
            if(accountId.isEmpty()){
                throw new IllegalArgumentException("Header: user or request parameter: accountId not present");
            }
            TransactionDto transactionDto = transactionService.getAllTransactions(accountId);
            if(transactionDto == null){
                throw new Exception("No Result found");
            }
            return ResponseEntity.ok().body(new CustomResponse(200, new ResponseDetails("Search Successful", transactionDto.getTransactionList()), null));
        }catch (IllegalArgumentException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails(e.getMessage(), null)));
        }
        catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails(e.getMessage(), null)));
        }

    }

}
