package com.mybillbook.web.controller;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mybillbook.web.constants.BillAccount;
import com.mybillbook.web.model.BillAccount.BillAccountDto;
import com.mybillbook.web.model.BillAccount.BillAccountReq;
import com.mybillbook.web.model.Response.CustomResponse;
import com.mybillbook.web.model.Response.ResponseDetails;
import com.mybillbook.web.model.Trsnsaction.TransactionDto;
import com.mybillbook.web.services.billAccount.BillAccountService;
import com.mybillbook.web.services.transaction.TransactionService;
import com.mybillbook.web.services.user.Userservices;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/account")
@CrossOrigin(origins = "http://localhost:3000")
public class BillAccountController {

    @Autowired
    BillAccountService billAccountService;

    @Autowired
    Userservices userservices;

    @Autowired
    TransactionService transactionService;


    @Autowired
    private RestTemplate restTemplate;


    @RequestMapping(method = RequestMethod.POST, value = "/create")
    private ResponseEntity<?> createBillAccount(@RequestHeader(name = "user") String user,
                                              @RequestBody @Valid BillAccountDto billAccount){
        try{
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            billAccount.setAccountCreatedOn(LocalDateTime.now().format(format));

            boolean isAdminExist = userservices.ifUsersExists(billAccount.getAccountAdminList());
            if(!isAdminExist){
                throw new UsernameNotFoundException("Admin user does not exist!");
            }
            billAccount.getAccountUserList().addAll(billAccount.getAccountAdminList());


            BillAccountDto newBillAccount = billAccountService.save(billAccount);
            if(newBillAccount.getAccountId().isEmpty()){
                throw new Exception("Bill Account not created");
            }
            TransactionDto transactionDto = new TransactionDto();
            transactionDto.setLinkedBillAccount(newBillAccount.getAccountId());
            transactionDto.setTransactionList(Arrays.asList());
            TransactionDto newAccountTransaction = transactionService.createTransaction(transactionDto);
            if(newAccountTransaction.getLinkedBillAccount().isEmpty()){
                ResponseEntity<DeleteResult> deleteResposne = restTemplate.getForEntity("/delete"+newBillAccount.getAccountId(), DeleteResult.class);
            }
            return ResponseEntity.ok().body(new CustomResponse(200, new ResponseDetails("Account Created Successfully", newBillAccount), null));
        }

        catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse(HttpStatus.NOT_FOUND.value(), null, new ResponseDetails(e.getMessage(), null)));
        }
    }


    @PutMapping(value = "/updateMetadata")
    ResponseEntity<?> updateBillAccount(@RequestBody BillAccountReq billAccountReq,
                                        @RequestHeader("user") String updatedby){
        try{
            Map<String,Object> updateBillAccountReq = billAccountReq.getBillAccount();
            BillAccountDto existingAccount = billAccountService.getBillAccountById(billAccountReq.getBillAccountId()).orElseThrow(()->
                    new Exception("Bill Account does not exist!")
            );
            if(!existingAccount.getAccountAdminList().contains(updatedby)){
                throw new Exception("The User does not have permission for the service");
            }
/*            if(updateBillAccountReq.keySet().contains(BillAccount.UnUpdatables.getAttributes())){
                throw new Exception("Some fields are not updatable");
            }*/
            UpdateResult updateResponse = billAccountService.updateAccount( billAccountReq.getBillAccountId(),updateBillAccountReq);
            if(updateResponse.getMatchedCount()==0){
                throw new Exception("No matched data");
            }
            return ResponseEntity.status(HttpStatus.OK).body(new CustomResponse(HttpStatus.OK.value(), new ResponseDetails("Updated Successfully", updateResponse), null));
        }
        catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails("Update failed", e.getMessage())));
        }
    }


    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/search")
    private ResponseEntity<?> search(@RequestParam(required = false) HashMap<String,Object> searchReq){
        try{
            if(searchReq.isEmpty()){
                throw new Exception("No index found for search");
            }
            if(!BillAccount.SearchAttributes.getAttributes().containsAll(searchReq.keySet())){
                throw new Exception("Invalid search attributes. Supported attributes are: "+BillAccount.SearchAttributes.getAttributes().toString());
            }

            List<BillAccountDto> searchRes = billAccountService.searchBillAccount(searchReq);
            if(searchRes.size()==0){
                throw new Exception("No result found for the search");
            }
            return ResponseEntity.status(HttpStatus.OK).body(new CustomResponse(HttpStatus.OK.value(), new ResponseDetails("Search Successful", searchRes), null));
        }
        catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails("Search failed", e.getMessage())));
        }
    }



    @DeleteMapping(value = "/delete/{billAccountNumber}")
    private ResponseEntity<?> delete(@PathVariable("billAccountNumber") String billAccountNumber,
                                     @RequestHeader("user") String user ){
        try{
            BillAccountDto existingAccount = billAccountService.getAccountAdmins(billAccountNumber);
            if(!existingAccount.getAccountAdminList().contains(user)){
                throw new UsernameNotFoundException("User doesnot have permission for the request");
            }
            DeleteResult deleteResposne = billAccountService.deleteAccount(billAccountNumber);
            if(deleteResposne.getDeletedCount()==0){
                throw new Exception("Account not found");
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), new ResponseDetails("Delete Successful", deleteResposne), null));
        }
        catch (IllegalArgumentException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails("Delete failed", "Invalid BillAccount Id")));
        }
        catch (UsernameNotFoundException u){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new CustomResponse(HttpStatus.FORBIDDEN.value(), null, new ResponseDetails("Delete failed", u.getMessage())));
        }
        catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse(HttpStatus.BAD_REQUEST.value(), null, new ResponseDetails("Delete failed", e.getMessage())));
        }
    }
}
